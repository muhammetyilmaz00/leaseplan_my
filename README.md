Tech Stack: Serenity Java, Maven, Cucumber

API Endpoints:

https://waarkoop-server.herokuapp.com/api/v1/search/test/apple
https://waarkoop-server.herokuapp.com/api/v1/search/test/mango
https://waarkoop-server.herokuapp.com/api/v1/search/test/car

How to run?

Open command prompt and go to application folder
Write "mvn verify" and hit enter
If you want to run specific feature, get the feature tag and set it into tags in TestRunner class

How to add new tests?

Create a new feature file under features.search package and write new cucumber scenarios