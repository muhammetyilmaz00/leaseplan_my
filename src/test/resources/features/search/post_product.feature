Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  @smoke
  Scenario: As a user I should see the fruits in the waarkoop server
    When I call endpoint "apple"
    Then I see the results displayed for "apple"
    When I call endpoint "mango"
    Then I see the results displayed for "mango"

  @smoke
  Scenario: As a user I should not see the non-fruits in the waarkoop server
    When I call endpoint "car"
    Then I do not see the results