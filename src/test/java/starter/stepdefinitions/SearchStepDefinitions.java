package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Assert;
import starter.pojos.CarsAPI;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    private EnvironmentVariables environmentVariables;

    @Steps
    CarsAPI carsAPI;

    @When("I call endpoint {string}")
    public void iCallEndpoint(String endpoint) {
        String theRestApiBaseUrl = environmentVariables.optionalProperty("restapi.baseurl")
                .orElse("https://waarkoop-server.herokuapp.com/api/v1/search/test/");

        SerenityRest.given().get(theRestApiBaseUrl + endpoint);
    }

    @Then("I do not see the results")
    public void i_Do_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error", is(true)));
        restAssuredThat(response -> response.body("detail.requested_item", is(carsAPI.requestedItem())));
    }

    @Then("I see the results displayed for {string}")
    public void iSeeTheResultsDisplayedFor(String product) {
        restAssuredThat(response -> response.statusCode(200));
        try {
            restAssuredThat(response -> response.body("title", everyItem(containsStringIgnoringCase(product))));
        } catch (Exception exception) {
            try {
                restAssuredThat(response -> response.body("brand", everyItem(containsStringIgnoringCase(product))));
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Product name " + product + " not found either title or brand");
            }
        }
    }
}
