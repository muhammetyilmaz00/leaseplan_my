package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "html:test-results/cucumber/cucumber-html-report.html",
                "json:test-results/cucumber/cucumber-json-report.json",},
        features = "src/test/resources/features",
        tags = "@smoke",
        glue = "/starter/stepdefinitions"
)
public class TestRunner {}
